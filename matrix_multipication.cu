// Matrix Multiplication for GPU
// Kevin Zagalo - Ismail Benkirane

#include<cuda.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 512
#define BATCH_SIZE 16

// L'idée ici est simple : il est inutile de stocker des matrices qui
// contiennent essentiellement des 0 et des 1. On garde seulement deux entiers
// i1 et i2 tels que J[i1][i2] = sin(theta), J[i1][i1] = cos(theta),
// J[i2][i1] = - sin(theta) et J[i2][i2] = cos(theta).
// On définit donc la structure suivante :

typedef struct {
	int i1;
	int i2;
	float theta;
} jacobi;

// Initialisation aléatoire des matrices A et B
void init_matrix(float *M) {
	for (int i = 0; i < BATCH_SIZE * BLOCK_SIZE; i++) {
		for (int j = 0; j < BLOCK_SIZE; j++) {
			M[i][j] = rand() % 10;
		}
	}
}

// On initialise à 0 la matrice des résultats
void init_matrix_zero(float *M) {
	for (int i = 0; i < BATCH_SIZE * BLOCK_SIZE; i++) {
		for (int j = 0; j < BLOCK_SIZE; j++) {
			M[i][j] = 0.0;
		}
	}
}

// transposition de toutes les matrices du batch
void transpose(float *M) {
	float tmp;
	for(int batch = 0 ; batch < BATCH_SIZE ; batch += BLOCK_SIZE) {
		for(int i = 0 ; i < BLOCK_SIZE ; i++) {
			for(int j = i+1 ; j < BLOCK_SIZE ; j++) {
				tmp = M[i+batch][j];
				M[i+batch][j] = M[j+batch][i];
				M[j+batch][i] = tmp;
			}
		}
	}
}

// allocation en mémoire gpu et transfert de la matrice M dans le gpu
void allocate_and_cpy(float *M, float *Mgpu) {
	cudaMalloc(&Mgpu, BATCH_SIZE * BLOCK_SIZE * BLOCK_SIZE * sizeof(float));
	cudaMemcpy(Mgpu, M, BATCH_SIZE * BLOCK_SIZE * BLOCK_SIZE * sizeof(float), cudaMemcpyHostToDevice);
}

// transfert la matrice du gpu vers la RAM puis détruit la matrice de la mémoire du gpu
void cpy_and_free(float *M, float *Mgpu) {
	cudaMemcpy(M, Mgpu, BATCH_SIZE * BLOCK_SIZE * BLOCK_SIZE * sizeof(float), cudaMemcpyDeviceToHost);
	cudaFree(Mgpu);
}

// produit matriciel sequentiel classique sur cpu
void cpu_prod(float *a, float *b, float *res) {
	for (int i = 0; i < BLOCK_SIZE; i++) {
		for (int j = 0; j < BLOCK_SIZE; j++) {
			for(int k = 0; k < BLOCK_SIZE; k++) {
				for(int l = 0; l < BATCH_SIZE * BLOCK_SIZE; l += BLOCK_SIZE) {
					res[i+l][j] += a[i+l][k] * b[k+l][j];
				}
			}
		}
	}
}

// kernel pour un produit ligne-colonne de deux lots de matrices carrées
__global__ void gpu_prod(float *A, float *B, float *res) {

	int bx = blockIdx.x;
	int tx = threadIdx.x;
	int idx = bx * BLOCK_SIZE + tx;

	float sum;

	for(int blk = 0 ; blk < BATCH_SIZE ; blk++) {

		// pour chaque block on "debatch", i.e. on associe chaque matrice du batch
		// à un block, ce qui facilite grandement l'écriture de l'algo et permet de
		// réduire considérablement le temps de calcul puisqu'on utilise uniquement
		// la mémoire gpu pour faire nos opérations.
		__shared__ float aTile[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ float bTile[BLOCK_SIZE][BLOCK_SIZE];

		for(int k = 0 ; k < BLOCK_SIZE ; k++) {
			aTile[tx][k] = A[idx][k];
			bTile[tx][k] = B[idx][k];
		}
		__syncthreads();

		// chaque thread de chaque block s'occupe d'une ligne de chaque matrice
		for(int i = 0 ; i < BLOCK_SIZE ; i++) {
			sum = 0.0f;
			for(int j = 0 ; j < BLOCK_SIZE ; j++) {
				sum += aTile[tx][j] * bTile[j][i];
			}
			// pas besoin de __syncthreads() puisqu'on utilise la shared memory
			res[idx][i] = sum;
		}
	}
}

// kernel pour un produit ligne-ligne de deux lots de matrices carrées
__global__ void gpu_prodbyrow(float *A, float *B, float *res) {

	int bx = blockIdx.x;
	int tx = threadIdx.x;
	int idx = bx * BLOCK_SIZE + tx;

	float sum;

	// pour chaque block on "debatch", i.e. on associe chaque matrice du batch
	// à un block, ce qui facilite grandement l'écriture de l'algo et permet de
	// réduire considérablement le temps de calcul puisqu'on utilise uniquement
	// la mémoire gpu pour faire nos opérations.
	for(int blk = 0 ; blk < BATCH_SIZE ; blk++) {
		__shared__ float aTile[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ float bTile[BLOCK_SIZE][BLOCK_SIZE];

		for(int k = 0 ; k < BLOCK_SIZE ; k++) {
			aTile[tx][k] = A[idx][k];
			bTile[tx][k] = B[idx][k];
		}
		__syncthreads();

		// chaque thread de chaque block s'occupe d'une ligne de chaque matrice
		for(int i = 0 ; i < BLOCK_SIZE ; i++) {
			sum = 0.0f;
			for(int j = 0 ; j < BLOCK_SIZE ; j++) {
				sum += aTile[tx][j] * bTile[i][j];
			}
			// pas besoin de __syncthreads() puisqu'on utilise la shared memory
			res[idx][i] = sum;
		}
	}
}

int main() {

	float timer,before,after;
	cudaEvent_t start, stop;
	float *aGPU, *bGPU;

	float A[BLOCK_SIZE * BATCH_SIZE][BLOCK_SIZE];
	float B[BLOCK_SIZE * BATCH_SIZE][BLOCK_SIZE];
	float res[BLOCK_SIZE * BATCH_SIZE][BLOCK_SIZE];

	init_matrix(&A, BLOCK_SIZE * BATCH_SIZE, BLOCK_SIZE);
	init_matrix(&B, BLOCK_SIZE * BATCH_SIZE, BLOCK_SIZE);
	init_matrix_zero(&res, BLOCK_SIZE * BATCH_SIZE, BLOCK_SIZE);

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// Multiplication matricielle sequentielle sur cpu
	cudaEventRecord(start,0);
	cpu_prod(&A, &B, &res);
	cudaEventRecord(stop,0);

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timer,start,stop);

	printf("Matrix multiplication on CPU took %1.2f seconds\n", timer);

	// allocation en mémoire gpu et transfert de la matrice M dans le gpu
	allocate_and_cpy(&A,&aGPU);
	allocate_and_cpy(&B,&bGPU);

	// Multiplication matricielle parallelisée sur gpu
	// produit matriciel ligne-colonne
	init_matrix_zero(&res, BLOCK_SIZE * BATCH_SIZE, BLOCK_SIZE);

	cudaEventRecord(start,0);
	gpu_prod<<<BATCH_SIZE, BLOCK_SIZE, BLOCK_SIZE*BLOCK_SIZE>>>(aGPU, bGPU, res);
	cudaEventRecord(stop,0);

	cudaDeviceSynchronize();
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timer,start,stop);
	printf("Matrix multiplication row-by-column on GPU took %1.2f seconds\n", timer);


	// produit matriciel ligne-ligne
	init_matrix_zero(&res,BLOCK_SIZE * BATCH_SIZE,BLOCK_SIZE);
	transpose(&bGPU);

	cudaEventRecord(start,0);
	gpu_prodbyrow<<<BATCH_SIZE,BLOCK_SIZE,BLOCK_SIZE*BLOCK_SIZE>>>(aGPU,bGPU,res);
	cudaEventRecord(stop,0);

	cudaDeviceSynchronize();

	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timer,start, stop);
	printf("Matrix multiplication row-by-row on GPU took %1.2f seconds\n", timer);

	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	// transfert la matrice du gpu vers la RAM et destruction des matrices de la mémoire du gpu
	cpy_and_free(&A, &aGPU);
	cudaFree(bGPU);

	return 0;
}
